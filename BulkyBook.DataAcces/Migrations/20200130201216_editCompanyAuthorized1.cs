﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BulkyBook.DataAcces.Migrations
{
    public partial class editCompanyAuthorized1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsAuthorizedCompany",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsAuthorizedCompany",
                table: "Companies",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
