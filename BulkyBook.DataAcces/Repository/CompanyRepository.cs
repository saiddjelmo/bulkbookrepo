﻿using BulkyBook.DataAcces.Data;
using BulkyBook.DataAcces.Repository.IRepository;
using BulkyBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulkyBook.DataAcces.Repository
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        private readonly ApplicationDbContext _db;

        public CompanyRepository(ApplicationDbContext db): base(db)
        {
            _db = db;
        }

        public void Update(Company company)
        {
            var objFromDb = _db.Companies.FirstOrDefault(c => c.CompanyId == company.CompanyId);

            if(objFromDb != null)
            {
                objFromDb.Name = company.Name;
                objFromDb.City = company.City;
                objFromDb.IsAuthorizedCompany = company.IsAuthorizedCompany;
                objFromDb.PhoneNumber = company.PhoneNumber;
                objFromDb.PostalCode = company.PostalCode;
                objFromDb.State = company.State;
                objFromDb.StreetAddress = company.StreetAddress;
                
            }

        }
    }
}
