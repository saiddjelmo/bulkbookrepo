﻿using BulkyBook.DataAcces.Data;
using BulkyBook.DataAcces.Repository.IRepository;
using BulkyBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulkyBook.DataAcces.Repository
{
    public class AplicationUserRepository : Repository<AplicationUser>, IAplicationUserRepository
    {
        private readonly ApplicationDbContext _db;

        public AplicationUserRepository(ApplicationDbContext db): base(db)
        {
            _db = db;
        }

    }
}
