﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BulkyBook.Utility
{
    public static class SD
    {
        public const string SP_CoverType_Create = "usp_CreateCoverType";
        public const string SP_CoverType_Get = "usp_GetCoverType";
        public const string SP_CoverType_GetAll = "usp_GetCoverTypes";
        public const string SP_CoverType_Update = "usp_UpdateCoverType";
        public const string SP_CoverType_Delete = "usp_DeleteCoverType";

        public const string Role_User_Indi = "Individual Customer";
        public const string Role_User_Comp = "Company Customer";
        public const string Role_Admin = "Admin";
        public const string Role_Employee = "Employee";





    }
}
