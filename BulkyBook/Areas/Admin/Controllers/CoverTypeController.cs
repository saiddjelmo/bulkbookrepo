﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BulkyBook.DataAcces.Repository.IRepository;
using BulkyBook.Models;
using Microsoft.AspNetCore.Mvc;
using BulkyBook.Utility;
using Dapper;
using Microsoft.AspNetCore.Authorization;

namespace BulkyBook.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class CoverTypeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CoverTypeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Upsert(int? id)
        {
            CoverType CoverType = new CoverType();
            if (id == null)
            {
                //for new CoverType - insert
                return View(CoverType);
            }

            var parameter = new DynamicParameters();
            parameter.Add("@Id", id);
            CoverType = _unitOfWork.SP_Call.OneRecord<CoverType>(SD.SP_CoverType_Get, parameter);

            if (CoverType == null)
            {
                return NotFound();
            }
            return View(CoverType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(CoverType CoverType)
        {
            if(ModelState.IsValid)
            {
                var param = new DynamicParameters();
                param.Add("@Name", CoverType.Name);
                if (CoverType.CoverTypeId == 0)
                {
                    
                    _unitOfWork.SP_Call.Execute(SD.SP_CoverType_Create, param);
                }
                else
                {
                    param.Add("@Id", CoverType.CoverTypeId);
                    _unitOfWork.SP_Call.Execute(SD.SP_CoverType_Update, param);
                }
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(CoverType);
        }


        #region API Calls

        [HttpGet]
        public  ActionResult GetAll()
        {
            var objForReturn = _unitOfWork.SP_Call.List<CoverType>(SD.SP_CoverType_GetAll, null);
            return Json(new { data = objForReturn} );
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var parameter = new DynamicParameters();
            parameter.Add("@Id", id);
            var objFromDb = _unitOfWork.SP_Call.OneRecord<CoverType>(SD.SP_CoverType_Get, parameter);
            if(objFromDb == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.SP_Call.Execute(SD.SP_CoverType_Delete, parameter);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Delete Successful" });
        }

        #endregion
    }
}